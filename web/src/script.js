$(document).ready(function () {
    var request = window.superagent;

    // Convert response from serializeArray to key value pairs
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    var form = function(callback) {
        callback(null, $('form').serializeObject());
    };

    var getCourseOccation = function(form, callback) {
        request
            .get(apiUrl + '/coursecheck/' + form.kurskod + '/' + form.termin)
            .end(function(error, response) {
                if (error) {
                    // Abort and show a short message
                    alert('Kurstillfället hittades inte');
                } else {
                    // Call the next function with data from the response
                    callback(null, form, response.body.anmkod);
                }
        });
    };

    var checkStudentOnCourse = function(form, anmkod, callback) {
        request
            .get(apiUrl + '/studentcheck/' + anmkod + '/' + form.student_id)
            .end(function(error, response) {
                if (error) {
                    // Abort and show a message
                    alert('Studenten finns inte registrerad på det här kurstillfället');
                } else {
                    // Continue to the next function
                    callback(null, form, anmkod);
                }
        });
    };

    var saveAssignmentResult = function(form, anmkod, callback) {
        request
            .put(apiUrl + '/studentreg/' + form.student_id + '/' + anmkod + '/' + form.provnummer + '/' + form.betyg)
            .end(function(error, response) {
                if (error) {
                    // An unknown error occured, show a message
                    alert('Något gick fel, det gick inte att registrera resultatet');
                } else {
                    // Everything went ok
                    alert('Resultatet är registrerat');
                }
        });
    };

    $('form').on('submit', function() {
        async.waterfall([
            form,
            getCourseOccation,
            checkStudentOnCourse,
            saveAssignmentResult,
        ]);
        return false;
    });
});
