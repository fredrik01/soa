var db = require('../models');

exports.getCourseOccation = function(request, response, next) {
    // Try to find a row in the course table with the given code and semester
    db.course.find({
        where: {
            code: request.params.code,
            semester: request.params.semester,
        }
    }).then(function(course) {
        // Return the row if found and the status code 404 if not found
        if (course) {
            response.send(course);
        } else {
            response.send(404);
        }
        return next();
    });
};

exports.checkStudentOnCourse = function(request, response, next) {
    // Try to find a row in the "student on course" table with the given
    // application code (anmälningskod) and studentID
    db.student_on_course.find({
        where: {
            anmkod: request.params.anmkod,
            student_id: request.params.studentid,
        }
    }).then(function(student_on_course) {
        // Return status code 204 (No Content) if found and
        // status code 404 if not found
        if (student_on_course) {
            response.send(204);
        } else {
            response.send(404);
        }
        return next();
    });
};

exports.saveAssignmentResult = function(request, response, next) {
    // Try to find a row in the "student on course" table with the given
    // application code (anmälningskod) and studentID
    db.student_on_course.find({
        where: {
            anmkod: request.params.anmkod,
            student_id: request.params.studentid,
        }
    }).then(function(student_on_course) {
        // If the student was found with the given application code then
        // update or add info to the database and return status code 201,
        // otherwise return status code 404
        if (student_on_course) {
            student_on_course.excode = request.params.excode;
            student_on_course.grade = request.params.grade;
            student_on_course.save();
            response.send(201);
        } else {
            response.send(404);
        }
        return next();
    });
};
