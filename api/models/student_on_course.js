module.exports = function(sequelize, DataTypes) {
	var StudentOnCourse = sequelize.define('student_on_course', {
		student_id: {
			type: DataTypes.STRING,
			unique: 'student_id_anmkod',
		},
		anmkod: {
			type: DataTypes.STRING,
			unique: 'student_id_anmkod',
		},
		excode: DataTypes.STRING,
		grade: DataTypes.STRING,
	});
	return StudentOnCourse;
};
